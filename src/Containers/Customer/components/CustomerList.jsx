import {
  Button,
  Divider,
  Grid,
  InputAdornment,
  List,
  ListItem,
  makeStyles,
  Menu,
  Paper,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import ArrowDownwardTwoToneIcon from "@material-ui/icons/ArrowDownwardTwoTone";
import ArrowUpwardTwoToneIcon from "@material-ui/icons/ArrowUpwardTwoTone";
import RadioButtonCheckedTwoToneIcon from "@material-ui/icons/RadioButtonCheckedTwoTone";
import RadioButtonUncheckedTwoToneIcon from "@material-ui/icons/RadioButtonUncheckedTwoTone";
import SearchTwoToneIcon from "@material-ui/icons/SearchTwoTone";
import SettingsIcon from "@material-ui/icons/Settings";
import CustomerListTable from "Containers/Customer/components/CustomerListTable";
import React, { useState } from "react";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    margin: theme.spacing(2),
    padding: theme.spacing(3),
  },
}));

export default function CustomerList(props) {
  const {
    onAddCustomer,
    onEditCustomer,
    onDeleteCustomer,
    customerPagedList,
    isLoading,
    onPageChange,
    onSizeChange,
    onOrderChange,
    order,
    onSearch
  } = props;
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Paper className={classes.pageContent}>
        <Toolbar>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6} style={{ textAlign: "right" }}>
              <Typography variant="h6">لیست مشتریان</Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                startIcon={<AddCircleIcon />}
                variant="text"
                variant="contained"
                size="small"
                color="primary"
                onClick={() => onAddCustomer(0)}
              >
                <Typography>افزودن</Typography>
              </Button>
              <Button
                startIcon={<SettingsIcon />}
                variant="text"
                variant="contained"
                size="small"
                color="primary"
                onClick={handleClick}
              >
                <Typography>تنظیمات</Typography>
              </Button>
              <Menu
                anchorEl={anchorEl}
                keepMounted
                getContentAnchorEl={null}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorEl)}
                classes={{ list: "p-0" }}
                onClose={handleClose}
              >
                <div style={{ right: "5px", position: "absolute" }}>
                  تعداد نتایج
                </div>
                <List component="div">
                  <ListItem
                    style={{ marginTop: "10px" }}
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onSizeChange(10);
                    }}
                  >
                    <div>
                      {customerPagedList && customerPagedList.size === 10 && (
                        <RadioButtonCheckedTwoToneIcon />
                      )}
                      {(!customerPagedList || customerPagedList.size !== 10) && (
                        <RadioButtonUncheckedTwoToneIcon />
                      )}
                      <span className="font-size-md">
                        <b>۱۰</b> تعداد در هر صفحه
                      </span>
                    </div>
                  </ListItem>
                  <ListItem
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onSizeChange(20);
                    }}
                  >
                    <div>
                      {customerPagedList && customerPagedList.size === 20 && (
                        <RadioButtonCheckedTwoToneIcon />
                      )}
                      {(!customerPagedList || customerPagedList.size !== 20) && (
                        <RadioButtonUncheckedTwoToneIcon />
                      )}
                      <span className="font-size-md">
                        <b>۲۰</b> تعداد در هر صفحه
                      </span>
                    </div>
                  </ListItem>
                  <Divider />
                  <div style={{ right: "5px", position: "absolute" }}>
                    مرتب سازی
                  </div>
                </List>
                <List component="div">
                  <ListItem
                    style={{ marginTop: "10px" }}
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onOrderChange("asc");
                    }}
                  >
                    <div>
                      <ArrowUpwardTwoToneIcon />
                    </div>
                    <span>صعودی</span>
                  </ListItem>
                  <ListItem
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onOrderChange("desc");
                    }}
                  >
                    <div>
                      <ArrowDownwardTwoToneIcon />
                    </div>
                    <span>نزولی</span>
                  </ListItem>
                </List>
              </Menu>
            </Grid>

            <Grid item xs={12} sm={12} style={{ textAlign: "right" }}>
              <Divider />
              <TextField
                style={{ marginTop: "5px" }}
                fullWidth
                variant="outlined"
                size="small"
                placeholder="جستجوی مشتری (نام یا نام خانوادگی یا کد ملی)"
                onChange={onSearch}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchTwoToneIcon />
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>
          </Grid>
        </Toolbar>
        <CustomerListTable
          customerPagedList={customerPagedList}
          onAddCustomer={onAddCustomer}
          onEditCustomer={onEditCustomer}
          onDeleteCustomer={onDeleteCustomer}
          onPageChange={onPageChange}
        />
      </Paper>
    </>
  );
}
