import {
  Button,
  Divider,
  Grid,
  List,
  ListItem,
  makeStyles,
  Menu,
  Paper,
  Toolbar,
  Typography,
} from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import ArrowDownwardTwoToneIcon from "@material-ui/icons/ArrowDownwardTwoTone";
import ArrowUpwardTwoToneIcon from "@material-ui/icons/ArrowUpwardTwoTone";
import RadioButtonCheckedTwoToneIcon from "@material-ui/icons/RadioButtonCheckedTwoTone";
import RadioButtonUncheckedTwoToneIcon from "@material-ui/icons/RadioButtonUncheckedTwoTone";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SettingsIcon from "@material-ui/icons/Settings";
import React, { useState } from "react";
import GhestListTable from "./GhestListTable";
import { Link } from 'react-router-dom';
import { history } from "shared/helpers/APIUtils";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    margin: theme.spacing(2),
    padding: theme.spacing(3),
  },
}));

export default function GhestList(props) {
  const {
    ghestList,
    customerId,
    onAddGhest,
    onEditeGhest,
    onDeleteGhest,
    onPageChange,
    onSizeChange,
    onOrderChange,
    order,
  } = props;

  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Paper className={classes.pageContent}>
        <Toolbar>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6} style={{ textAlign: "right" }}>
              <Typography variant="h6">لیست اقساط</Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                startIcon={<AddCircleIcon />}
                variant="text"
                variant="contained"
                size="small"
                color="primary"
                onClick={() => onAddGhest(0)}
              >
                <Typography>افزودن</Typography>
              </Button>
              <Button
                startIcon={<SettingsIcon />}
                variant="text"
                variant="contained"
                size="small"
                color="primary"
                onClick={handleClick}
              >
                <Typography>تنظیمات</Typography>
              </Button>
              <Menu
                anchorEl={anchorEl}
                keepMounted
                getContentAnchorEl={null}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorEl)}
                classes={{ list: "p-0" }}
                onClose={handleClose}
              >
                <div style={{ right: "5px", position: "absolute" }}>
                  تعداد نتایج
                </div>
                <List component="div">
                  <ListItem
                    style={{ marginTop: "10px" }}
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onSizeChange(10);
                    }}
                  >
                    <div>
                      {ghestList && ghestList.length === 10 && (
                        <RadioButtonCheckedTwoToneIcon />
                      )}
                      {(!ghestList || ghestList.length !== 10) && (
                        <RadioButtonUncheckedTwoToneIcon />
                      )}
                      <span className="font-size-md">
                        <b>۱۰</b> تعداد در هر صفحه
                      </span>
                    </div>
                  </ListItem>
                  <ListItem
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onSizeChange(20);
                    }}
                  >
                    <div>
                      {ghestList && ghestList.length === 20 && (
                        <RadioButtonCheckedTwoToneIcon />
                      )}
                      {(!ghestList || ghestList.length !== 20) && (
                        <RadioButtonUncheckedTwoToneIcon />
                      )}
                      <span className="font-size-md">
                        <b>۲۰</b> تعداد در هر صفحه
                      </span>
                    </div>
                  </ListItem>
                  <Divider />
                  <div style={{ right: "5px", position: "absolute" }}>
                    مرتب سازی
                  </div>
                </List>
                <List component="div">
                  <ListItem
                    style={{ marginTop: "10px" }}
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onOrderChange("asc");
                    }}
                  >
                    <div>
                      <ArrowUpwardTwoToneIcon />
                    </div>
                    <span>صعودی</span>
                  </ListItem>
                  <ListItem
                    component="a"
                    button
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      onOrderChange("desc");
                    }}
                  >
                    <div>
                      <ArrowDownwardTwoToneIcon />
                    </div>
                    <span>نزولی</span>
                  </ListItem>
                </List>
              </Menu>
              {/* <Link to="" style={{ textDecoration: "none" }}> */}
                <Button
                  startIcon={<ArrowBackIcon />}
                  variant="text"
                  variant="contained"
                  size="small"
                  color="primary"
                  onClick={() => history.push(`/app/customer/${customerId}/bimeList`)}
                >
                  <Typography>بازگشت</Typography>
                </Button>
              {/* </Link> */}
            </Grid>
          </Grid>
        </Toolbar>
        <GhestListTable
          ghestList={ghestList}
          onEditeGhest={onEditeGhest}
          onDeleteGhest={onDeleteGhest}
        />
      </Paper>
    </>
  );
}
