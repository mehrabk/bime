import PeopleOutlineOutlinedIcon from "@material-ui/icons/PeopleOutlineOutlined";
import CustomerEditForm from "Containers/Customer/components/CustomerEditForm";
import CustomerList from "Containers/Customer/components/CustomerList";
import PageHeader from "Containers/Header/PageHeader";
import React, { useEffect, useState } from "react";
import ErrorConfirmationModal from "shared/components/modal/ErrorConfirmationModal";
import Modal from "shared/components/modal/Modal";
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE,
  DEFAULT_SORT_ORDER,
} from "shared/helpers/APIUtils";
import { useCustomer } from "shared/hooks/CustomerHooks";
import { useCustomers } from "shared/hooks/CustomerHooks";
import Notification from "../../shared/components/notification/Notification";
import { DEFAULT_PAGE_SORT } from '../../shared/helpers/APIUtils';

export default function Customer() {
  const [size, setSize] = useState(DEFAULT_PAGE_SIZE);
  const [page, setPage] = useState(DEFAULT_PAGE_NUMBER);
  const [sort, setSort] = useState(DEFAULT_PAGE_SORT);
  const [order, setOrder] = useState(DEFAULT_SORT_ORDER);
  const [query, setQuery] = useState("");

  const [modal, setModal] = useState(false);
  const [showNotification, setShowNotification] = useState({
    isOpen: false,
    message: "",
    type: "",
  });
  const [confirmModal, setConfirmModal] = useState({
    isOpen: false,
  });
  const [customerId, setCustomerId] = useState();

  const [
    customerPagedList,
    onUpdateCustomer,
    onDeleteCustomer,
    { errorMsg, isLoading },
  ] = useCustomer(page, size, query, order, sort);

  console.log(customerPagedList);

  useEffect(() => {
    if (errorMsg && errorMsg !== "") {
      setShowNotification({
        isOpen: true,
        message: errorMsg,
        type: "error",
      });
    }
  }, [errorMsg]);

  const handleAddCustomer = (cId) => {
    console.log("handleAdd => ", cId);
    setCustomerId(cId);
    setModal(true);
  };

  const handleEditCustomer = (cId) => {
    console.log("handleEdit => ", cId);
    setCustomerId(cId);
    setModal(true);
  };

  const handleDeleteCustomer = (customer) => {
    console.log("handleDelete => ", customer.id);
    setConfirmModal({
      isOpen: true,
      onConfirm: () => {
        onDeleteCustomer(customer);
        setConfirmModal({
          isOpen: false,
        });
      },
    });
  };

  return (
    <>
      <PageHeader
        title="پنل مدیریت مشتریان"
        subTitle="مدیریت مشتریان / ایجاد و حذف قراردادها و ..."
        icon={<PeopleOutlineOutlinedIcon fontSize="large" />}
      />

      <CustomerList
        onAddCustomer={handleAddCustomer}
        onEditCustomer={handleEditCustomer}
        onDeleteCustomer={handleDeleteCustomer}
        customerPagedList={customerPagedList}
        isLoading={isLoading}
        onPageChange={(p) => setPage(p - 1)}
        onSizeChange={(s) => setSize(s)}
        onOrderChange={(o) => setOrder(o)}
        order={order}
        onSearch={(e) => setQuery(e.target.value)}
      />

      <Modal
        openModal={modal}
        closeModal={() => setModal(!modal)}
        title={customerId > 0 ? "ویرایش اطلاعات مشتری" : "مشتری جدید"}
      >
        <CustomerEditForm
          customerId={customerId}
          onUpdateCustomer={onUpdateCustomer}
          handleClose={() => setModal(!modal)}
        />
      </Modal>

      <Notification
        showNotification={showNotification}
        onClose={() =>
          setShowNotification({ ...showNotification, isOpen: false })
        }
      />

      <ErrorConfirmationModal
        confirmModal={confirmModal}
        closeConfirmModal={() => setConfirmModal({ isOpen: false })}
      />
    </>
  );
}
