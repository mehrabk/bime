import { Switch, Route, Redirect, useLocation } from "react-router-dom";
import React from "react";
import { useSelector } from "react-redux";
import Customer from "Containers/Customer";
import Ghest from "Containers/Ghest";
import Bime from '../../../Containers/Bime/index';

import Profile from "Containers/Auth/Profile/profile";

export default function MyAppRouter() {
  const user = useSelector((state) => state.auth.username);
  return (
    <Switch>
      {user && (
        <>
          <Route exact path="/app" component={Customer} />
          <Route exact path="/app/customer/:customerId/bimeList" component={Bime} />
          <Route exact path="/app/customer/:customerId/bime/:bimeId/ghestList" component={Ghest} />
          <Route exact path="/app/profile" component={Profile} />
        </>
      )}
      {!user && <Redirect to='/login' />}
    </Switch>
  );
}
