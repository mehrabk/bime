import React from "react";
import { Grid, InputAdornment, TextField } from "@material-ui/core";

export default function LoginForm({ methods }) {
  return (
    <>
      <Grid item sm={12} xs={12}>
        <TextField
          fullWidth
          autoComplete="off"
          variant="outlined"
          id="textfield-email"
          label="نام کاربری"
          name="username"
          inputRef={methods.register}
          error={
            methods.errors &&
            methods.errors.username &&
            methods.errors.username.message
          }
          helperText={
            methods.errors &&
            methods.errors.username &&
            methods.errors.username.message
          }
        />
      </Grid>

      <Grid item sm={12} xs={12}>
        <TextField
          fullWidth
          autoComplete="off"
          variant="outlined"
          id="textfield-password"
          label="رمز عبور"
          type="password"
          name="password"
          inputRef={methods.register}
          error={
            methods.errors &&
            methods.errors.password &&
            methods.errors.password.message
          }
          helperText={
            methods.errors &&
            methods.errors.password &&
            methods.errors.password.message
          }
        />
      </Grid>
    </>
  );
}
