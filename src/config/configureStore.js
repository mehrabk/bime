import { combineReducers } from "redux";
import authReducer from "../redux/reducers/authReducer";

export default combineReducers({
  auth: authReducer,
});