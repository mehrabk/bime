import {
  AUTHENTICATE_SUCCESS,
  AUTHENTICATE_FAIL,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
} from "./types";

export function auth(user) {
  return {
    type: AUTHENTICATE_SUCCESS,
    payload: user
  }
}
