import PageHeader from "Containers/Header/PageHeader";
import React, { useEffect, useState } from "react";
import PeopleOutlineOutlinedIcon from "@material-ui/icons/PeopleOutlineOutlined";
import Modal from "shared/components/modal/Modal";
import Notification from "shared/components/notification/Notification";
import ErrorConfirmationModal from "shared/components/modal/ErrorConfirmationModal";
import { useGhest } from "../../shared/hooks/GhestHooks";
import GhestList from "./components/GhestList";
import { DEFAULT_PAGE_SIZE } from "shared/helpers/APIUtils";
import { DEFAULT_PAGE_NUMBER } from "shared/helpers/APIUtils";
import { DEFAULT_PAGE_SORT } from "shared/helpers/APIUtils";
import { DEFAULT_SORT_ORDER } from "shared/helpers/APIUtils";
import GhestEditForm from "./components/GhestEditForm";

export default function Ghest({ match }) {
  const { customerId, bimeId } = match.params;
  const [modal, setModal] = useState(false);
  const [size, setSize] = useState(DEFAULT_PAGE_SIZE);
  const [page, setPage] = useState(DEFAULT_PAGE_NUMBER);
  const [sort, setSort] = useState(DEFAULT_PAGE_SORT);
  const [order, setOrder] = useState(DEFAULT_SORT_ORDER);
  const [ghestId, setGhestId] = useState();
  const [
    ghestList,
    onUpdateGhest,
    onDeleteGhest,
    { errorMsg, isLoading },
  ] = useGhest(bimeId, size, page, sort, order);
  const [showNotification, setShowNotification] = useState({
    isOpen: false,
    message: "",
    type: "",
  });
  const [confirmModal, setConfirmModal] = useState({
    isOpen: false,
  });

  console.log(ghestList);

  useEffect(() => {
    if (errorMsg && errorMsg !== "") {
      setShowNotification({
        isOpen: true,
        message: errorMsg,
        type: "error",
      });
    }
  }, [errorMsg]);

  const handleAddGhest = (id) => {
    setGhestId(id);
    setModal(true);
  };

  const handleEditGhest = (id) => {
    setGhestId(id);
    setModal(true);
  };

  const handleDeleteGhest = (ghest) => {
    setConfirmModal({
      isOpen: true,
      onConfirm: () => {
        onDeleteGhest(ghest);
        setConfirmModal({
          isOpen: false,
        });
      },
    });
  };

  return (
    <>
      <PageHeader
        title="اقساط بیمه"
        subTitle="مدیریت اقساط قراردادها"
        icon={<PeopleOutlineOutlinedIcon fontSize="large" />}
      />

      <GhestList
        ghestList={ghestList}
        customerId={customerId}
        onAddGhest={handleAddGhest}
        onEditeGhest={handleEditGhest}
        onDeleteGhest={handleDeleteGhest}
        onPageChange={(p) => setPage(p - 1)}
        onSizeChange={(s) => setSize(s)}
        onOrderChange={(o) => setOrder(o)}
        order={order}
      />

      <Modal
        openModal={modal}
        closeModal={() => setModal(!modal)}
        title={ghestId > 0 ? "ویرایش اطلاعات قسط" : "قسط جدید"}
      >
        <GhestEditForm
          ghestId={ghestId}
          bimeId={bimeId}
          onUpdateGhest={onUpdateGhest}
          handleClose={() => setModal(!modal)}
        />
      </Modal>

      <Notification
        showNotification={showNotification}
        onClose={() =>
          setShowNotification({ ...showNotification, isOpen: false })
        }
      />

      <ErrorConfirmationModal
        confirmModal={confirmModal}
        closeConfirmModal={() => setConfirmModal({ isOpen: false })}
      />
    </>
  );
}
