import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import React from "react";
import EditTwoToneIcon from "@material-ui/icons/EditTwoTone";
import IconButton from "@material-ui/core/IconButton";
import DeleteForeverTwoToneIcon from "@material-ui/icons/DeleteForeverTwoTone";
import { history } from "shared/helpers/APIUtils";

const tableHeadCells = [
  { id: "bimeNumber", label: "شماره بیمه" },
  { id: "yektaCode", label: "کد یکتا" },
  { id: "type", label: "نوع قرارداد" },
  // {id: "createdAt", label: "تاریخ عقد قرارداد"},
  // {id: "updatedAt", label: "تاریخ ویرایش قرارداد"},
  { id: "pishPardakht", label: "پیش پرداخت" },
  { id: "note", label: "توضیحات" },
  { id: "actions", label: "ویرایش/حذف" },
  { id: "ghestCount", label: "تعداد قسط" },
  { id: "installment", label: "اقساط" },
];

export default function BimeListTable(props) {
  const { bimeList, customerId, onEditBime, onDeleteBime } = props;
  return (
    <TableContainer component={Paper} style={{ marginTop: "10px" }}>
      <Table>
        <TableHead>
          {tableHeadCells.map((headCell) => (
            <TableCell key={headCell.id}>{headCell.label}</TableCell>
          ))}
        </TableHead>
        <TableBody>
          {bimeList &&
            bimeList.length > 0 &&
            bimeList.map((bime) => (
              <TableRow key={bime.id}>
                <TableCell>{bime.bimeNumber}</TableCell>
                <TableCell>{bime.yektaCode}</TableCell>
                <TableCell>{bime.type && bime.type.label}</TableCell>
                <TableCell>{bime.pishPardakht}</TableCell>
                <TableCell
                  style={{
                    overflow: "auto",
                    width: "200px",
                    height: "150px",
                  }}
                >
                  {bime.note}
                </TableCell>
                <TableCell>
                  <IconButton
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={() => {
                      onEditBime(bime);
                    }}
                  >
                    <EditTwoToneIcon />
                  </IconButton>
                  <IconButton
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={() => onDeleteBime(bime)}
                  >
                    <DeleteForeverTwoToneIcon />
                  </IconButton>
                </TableCell>

                <TableCell>
                  <Typography color="secondary">{bime.ghestCount}</Typography>
                </TableCell>

                <TableCell>
                  <IconButton
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={() =>
                      history.push(
                        `/app/customer/${customerId}/bime/${bime.id}/ghestList`
                      )
                    }
                  >
                    <EditTwoToneIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
