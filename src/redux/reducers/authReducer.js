import {
  AUTHENTICATE_SUCCESS,
  AUTHENTICATE_FAIL,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
} from "../actions/types";

const initialState = {username: "", roles: []};

export default function (state = initialState, action) {
  const { type, payload } = action;
  console.log("reducer user => ", payload)

  switch (type) {
    case AUTHENTICATE_SUCCESS:
      return {
        ...state,
        username: payload.username,
        roles: payload.roles
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
      };
    case REGISTER_FAIL:
      return {
        ...state,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        user: payload.user,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        user: null,
      };
    case LOGOUT:
      return {
        ...state,
        user: null,
      };
    default:
      return state;
  }
}
