import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import PeopleIcon from "@material-ui/icons/People";
import BarChartIcon from "@material-ui/icons/BarChart";
import LayersIcon from "@material-ui/icons/Layers";
import AssignmentIcon from "@material-ui/icons/Assignment";
import { Divider, makeStyles, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";

const useStyle = makeStyles((theme) => ({
  link: {
    underline: "none",
  },
}));

export const mainListItems = (
  <>
    <div style={{marginTop: "15px"}}>
      <Typography variant="h5" align="center">
        بیمه پارسیان
      </Typography>
    </div>
    
    <div style={{marginTop: "10px"}} >
    <Divider />
      <Link to="/dashboard" style={{textDecoration: 'none', color: "black"}}>
        <ListItem button>
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="میزکار" />
        </ListItem>
      </Link>

      <Link to="/contracts" style={{textDecoration: 'none', color: "black"}}>
        <ListItem button>
          <ListItemIcon>
            <ShoppingCartIcon />
          </ListItemIcon>
          <ListItemText primary="قراردادها" />
        </ListItem>
      </Link>

      <Link to="/app" style={{textDecoration: 'none', color: "black"}}>
        <ListItem button>
          <ListItemIcon>
            <PeopleIcon />
          </ListItemIcon>
          <ListItemText primary="مشتریان" />
        </ListItem>
      </Link>

      <Link to="/reports" style={{textDecoration: 'none', color: "black"}}>
        <ListItem button>
          <ListItemIcon>
            <BarChartIcon />
          </ListItemIcon>
          <ListItemText primary="گزارشات" />
        </ListItem>
      </Link>
    </div>
  </>
);

export const secondaryListItems = (
  <div>
    <Link to="/login" style={{textDecoration: 'none', color: "black"}}> 
      <ListItem button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="ورود به سایت" />
      </ListItem>
    </Link>

    <Link to="/register" style={{textDecoration: 'none', color: "black"}}>
      <ListItem button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="ثبت نام" />
      </ListItem>
    </Link>

    <Link to="/app/profile" style={{textDecoration: 'none', color: "black"}}>
      <ListItem button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="مشخصات کاربر" />
      </ListItem>
    </Link>
  </div>
);
