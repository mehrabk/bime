import {
    AppBar,
    Badge,
    Grid,
    IconButton,
    InputBase,
    makeStyles,
    Toolbar,
} from "@material-ui/core";
import React from "react";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import ChatBubbleOutlineIcon from "@material-ui/icons/ChatBubbleOutline";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "white",
        transform: "translateZ(0)",
        boxShadow:'none'
    },
    searchInput: {
        opacity: "0.7",
        padding: `0px ${theme.spacing(1.5)}px`,
        fontSize: "1rem",
        width: "100%",
        "&:hover": {
            backgroundColor: "#f2f2f2",
        },
        "& .MuiSvgIcon-root": {
            marginRight: theme.spacing(1.5),
        },
        "& .MuiInputBase-input": {
            color: "red",
            backgroundColor:"yellow"
        },
    },
    btnRoot: {
        backgroundColor: "blue",
        // "&:hover": {backgroundColor:"green"},
        // "& .MuiBadge-badge": {color:"green", backgroundColor: "pink"}
    },
    btnLabel: {
        backgroundColor: "brown",
        "&:hover": {backgroundColor:"pink"},
        "& .MuiBadge-badge": {color:"green", backgroundColor: "pink", "&:hover": {color:"black"}}
    },
}));

export default function Header() {
       const classes = useStyles();

    return (
        <AppBar position="static" className={classes.root}>
            <Toolbar>
                <Grid container alignItems="center">
                    {/* <Grid item xs={12} sm={6}>
                        <InputBase
                            className={classes.searchInput}
                            placeholder="جست و جو"
                            startAdornment={<SearchIcon fontSize="small" />}
                        />
                    </Grid> */}
                    <Grid item sm></Grid>
                    <Grid item>
                        {/* <IconButton
                            classes={{
                                root: classes.btnRoot,
                                label: classes.btnLabel,
                            }}
                        >
                            <Badge badgeContent={4} color="secondary">
                                <NotificationsNoneIcon fontSize="small" />
                            </Badge>
                        </IconButton>

                        <IconButton>
                            <Badge badgeContent={3} color="primary">
                                <ChatBubbleOutlineIcon
                                    fontSize="small"
                                    color="secondary"
                                />
                            </Badge>
                        </IconButton> */}

                        <IconButton size="small" classes={{root: classes.btnRoot}}>
                            <PowerSettingsNewIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}
