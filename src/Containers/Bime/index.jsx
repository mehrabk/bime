import {
  Container,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import PeopleOutlineOutlinedIcon from "@material-ui/icons/PeopleOutlineOutlined";
import PageHeader from "Containers/Header/PageHeader";
import React, { useEffect, useState } from "react";
import ErrorConfirmationModal from "shared/components/modal/ErrorConfirmationModal";
import Modal from "shared/components/modal/Modal";
import Notification from "shared/components/notification/Notification";
import BimeList from "./components/BimeList";
import { useBime } from "../../shared/hooks/BimeHooks";
import BimeBadaneForm from "./components/BimeBadaneForm";
import BimeSalesForm from "./components/BimeSalesForm";

export default function Bime({ match }) {
  const customerId = match.params.customerId;
  const [modal, setModal] = useState(false);
  const [showNotification, setShowNotification] = useState({
    isOpen: false,
    message: "",
    type: "",
  });
  const [confirmModal, setConfirmModal] = useState({
    isOpen: false,
  });
  const [bimeId, setBimeId] = useState(0);
  const [bimeType, setBimeType] = useState(1);
  const handleChangeBimeType = (event) => {
    setBimeType(event.target.value);
  };

  const [
    bimeList,
    onUpdateBime,
    onDeleteBime,
    { errorMsg, isLoading },
  ] = useBime(customerId);

  console.log("bimeType ==> ", bimeType);

  useEffect(() => {
    if (errorMsg && errorMsg !== "") {
      setShowNotification({
        isOpen: true,
        message: errorMsg,
        type: "error",
      });
    }
  }, [errorMsg]);

  const handleAddBime = (id) => {
    console.log("add == > ", id);
    setBimeId(id);
    setModal(true);
  };

  const handleEditBime = (bime) => {
    console.log("edit == > ", bime.id);
    setBimeId(bime.id);
    setBimeType(bime.type.value)
    setModal(true);
  };

  const handleDeleteBime = (bime) => {
    if (bime.id > 0) {
      console.log("====== > delete method");
      setConfirmModal({
        isOpen: true,
        onConfirm: () => {
          onDeleteBime(bime);
          setConfirmModal({ isOpen: false });
        },
      });
    }
  };

  return (
    <>
      <PageHeader
        title="لیست بیمه / قراردادهای مشتری"
        subTitle="مدیریت بیمه های مشتری"
        icon={<PeopleOutlineOutlinedIcon fontSize="large" />}
      />

      <BimeList
        bimeList={bimeList}
        customerId={customerId}
        onAddBime={handleAddBime}
        onEditBime={handleEditBime}
        onDeleteBime={handleDeleteBime}
      />

      <Modal
        openModal={modal}
        closeModal={() => setModal(!modal)}
        title={bimeId > 0 ? "ویرایش قرارداد" : "ایجاد قرارداد"}
      >
        <Container style={{ textAlign: "right" }}>
          <FormControl component="fieldset">
            <RadioGroup row value={bimeType} onChange={handleChangeBimeType}>
              <FormControlLabel
                value="1"
                checked={bimeType == 1}
                control={<Radio />}
                label="بیمه ثالث"
                disabled={bimeId > 0 && bimeType == 2}
              />
              <FormControlLabel
                value="2"
                checked={bimeType == 2}
                control={<Radio />}
                label="بیمه بدنه"
                disabled={bimeId > 0 && bimeType == 1}
              />
            </RadioGroup>
          </FormControl>
          
          {bimeType && bimeType == 1 && (
            <BimeSalesForm
              customerId={customerId}
              bimeId={bimeId}
              onUpdateBime={onUpdateBime}
              handleClose={() => setModal(!modal)}
            />
          )}
          
          {bimeType && bimeType == 2 && (
            <BimeBadaneForm
              customerId={customerId}
              bimeId={bimeId}
              onUpdateBime={onUpdateBime}
              handleClose={() => setModal(!modal)}
            />
          )}

        </Container>
      </Modal>

      <Notification
        showNotification={showNotification}
        onClose={() =>
          setShowNotification({ ...showNotification, isOpen: false })
        }
      />

      <ErrorConfirmationModal
        confirmModal={confirmModal}
        closeConfirmModal={() => setConfirmModal({ isOpen: false })}
      />
    </>
  );
}
