import {
  Button,
  Divider,
  makeStyles,
  Paper,
  Toolbar,
  Typography,
} from "@material-ui/core";
import React from "react";
import { Grid } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ContractListTable from "./BimeListTable";
import { Link } from "react-router-dom";
import BimeListTable from "./BimeListTable";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    margin: theme.spacing(2),
    padding: theme.spacing(3),
  },
}));

export default function BimeList(props) {
  const {
    bimeList,
    customerId,
    onAddBime,
    onEditBime,
    onDeleteBime,
  } = props;
  const classes = useStyles();
  return (
    <>
      <Paper className={classes.pageContent}>
        <Toolbar>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6} style={{ textAlign: "right" }}>
              <Typography variant="h6">لیست قراردادهای مشتری </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                startIcon={<AddCircleIcon />}
                variant="text"
                variant="contained"
                size="small"
                color="primary"
                onClick={() => onAddBime(0)}
              >
                <Typography>ایجاد قرارداد</Typography>
              </Button>
              <Link to="/app" style={{ textDecoration: "none" }}>
                <Button
                  startIcon={<ArrowBackIcon />}
                  variant="text"
                  variant="contained"
                  size="small"
                  color="primary"
                >
                  <Typography>بازگشت</Typography>
                </Button>
              </Link>
            </Grid>
          </Grid>
        </Toolbar>
        <Divider />
        <BimeListTable
          bimeList={bimeList}
          customerId={customerId}
          onEditBime={onEditBime}
          onDeleteBime={onDeleteBime}
        />
      </Paper>
    </>
  );
}
