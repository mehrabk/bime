import React, { useEffect, useState } from "react";
import {
  Container,
  Grid,
  makeStyles,
  Paper,
  Button,
  Snackbar,
} from "@material-ui/core";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { request } from "shared/helpers/APIUtils";
import { ACCESS_TOKEN } from "shared/helpers/APIUtils";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { auth } from "redux/actions/authAction";

import { history } from "shared/helpers/APIUtils";
import PeopleOutlineIcon from "@material-ui/icons/PeopleOutline";
import LoginForm from "./components/LoginForm";
import PageHeader from "Containers/Header/PageHeader.js";
import BlockUi from "react-block-ui";
import { ScaleLoader } from "react-spinners";
import Notification from "../../../shared/components/notification/Notification";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    margin: theme.spacing(5),
    padding: theme.spacing(3),
    width: "40%",
    marginLeft: "30%",
  },
}));

const VALIDATION_SCHEMA = yup.object().shape({
  username: yup.string().required("ضرروری"),
  password: yup.string().required("ضرروری"),
});

function Login() {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const [showNotification, setShowNotification] = useState({
    isOpen: false,
    message: "",
    type: "",
  });
  const dispatch = useDispatch();
  const methods = useForm({ resolver: yupResolver(VALIDATION_SCHEMA) });

  const onSubmit = async ({ username, password }) => {
    setLoading(true);
    try {
      const response = await request().post("/auth/signin", {
        username,
        password,
      });
      console.log(response);
      localStorage.setItem(ACCESS_TOKEN, response.data.accessToken);
      const user = await request().get("/user/me");
      dispatch(auth(user.data));
      history.push("/app");
    } catch (error) {
      console.log(error);
      localStorage.removeItem(ACCESS_TOKEN);
      setErrorMsg("ورود شما با خطا مواجه شد.");
    }
    setLoading(false);
  };

  useEffect(() => {
    if (errorMsg && errorMsg !== "") {
      setShowNotification({
        isOpen: true,
        message: errorMsg,
        type: "error",
      });
    }
  }, [errorMsg]);
  return (
    <>
      <PageHeader
        title="ورود به سایت"
        subTitle="ورود مدیر و اعضا به سایت"
        icon={<PeopleOutlineIcon fontSize="large" />}
      />

      <Paper className={classes.pageContent}>
        <Container style={{ width: "100%" }}>
          <BlockUi
            tag="div"
            blocking={loading}
            loader={<ScaleLoader color={"var(--success)"} loading={loading} />}
          >
            <form onSubmit={methods.handleSubmit(onSubmit)}>
              <Grid container spacing={3}>
                <LoginForm methods={methods} />
              </Grid>
              <Grid item sm={12} xs={12}>
                <div style={{ textAlign: "center", marginTop: "10px" }}>
                  <Button
                    variant="contained"
                    color="primary"
                    style={{ width: "60px", height: "40px" }}
                    type="submit"
                  >
                    ورود
                  </Button>
                </div>
              </Grid>
            </form>
          </BlockUi>
        </Container>
      </Paper>

      <Notification
        showNotification={showNotification}
        onClose={() => {
          setShowNotification({
            isOpen: false,
            message: "",
            type: "",
          });
          setErrorMsg("");
        }}
      />
    </>
  );
}

export default Login;
