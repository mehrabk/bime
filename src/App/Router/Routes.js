import Login from "Containers/Auth/Login";
import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import MyAppRouter from "./component/MyAppRouter";

export const Routes = ({ loading }) => {
  
  return (
    <>
      {!loading && (
        <Switch>
          <Redirect exact from="/" to="/app/" />
          <Route exact path="/login" component={Login} />
          <Route path="/app" component={MyAppRouter} />
        </Switch>
      )}

    </>
  );

  // <Route exact path="/bime/register" component={Register} />
  // <Route exact path="/bime/profile" component={Profile} />
};
