import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import React from "react";

import EditTwoToneIcon from "@material-ui/icons/EditTwoTone";
import IconButton from "@material-ui/core/IconButton";
import DeleteForeverTwoToneIcon from "@material-ui/icons/DeleteForeverTwoTone";
import { history } from "shared/helpers/APIUtils";
import Pagination from "@material-ui/lab/Pagination";

const tableHeadCells = [
  { id: "userName", label: "نام" },
  { id: "lastName", label: "نام خانوادگی" },
  { id: "address", label: "آدرس" },
  { id: "phoneNumber", label: "شماره موبایل" },
  { id: "identityCode", label: "شماره ملی" },
  { id: "actions", label: "ویرایش/حذف" },
  { id: "contracts", label: "تعداد قرارداد" },
  { id: "contractsActions", label: "عملیات قراردادها" },
];

export default function CustomerListTable(props) {
  const {
    customerPagedList,
    onAddCustomer,
    onEditCustomer,
    onDeleteCustomer,
    onPageChange,
  } = props;

  return (
    <>
      <TableContainer component={Paper} style={{ marginTop: "10px" }}>
        <Table>
          <TableHead>
            {tableHeadCells.map((headCell) => (
              <TableCell key={headCell.id}>{headCell.label}</TableCell>
            ))}
          </TableHead>
          <TableBody>
            {customerPagedList &&
              customerPagedList.content &&
              customerPagedList.content.map((customer) => (
                <TableRow key={customer.id}>
                  <TableCell>{customer.userName}</TableCell>
                  <TableCell>{customer.lastName}</TableCell>
                  <TableCell>{customer.address}</TableCell>
                  <TableCell>{customer.phoneNumber}</TableCell>
                  <TableCell>{customer.identityCode}</TableCell>
                  <TableCell>
                    <IconButton
                      variant="contained"
                      color="primary"
                      size="small"
                      onClick={() => onEditCustomer(customer.id)}
                    >
                      <EditTwoToneIcon />
                    </IconButton>
                    <IconButton
                      variant="contained"
                      color="primary"
                      size="small"
                      onClick={() => onDeleteCustomer(customer)}
                    >
                      <DeleteForeverTwoToneIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell>
                    <Typography color="secondary">
                      {customer.bimeCount}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <IconButton
                      variant="contained"
                      color="primary"
                      size="small"
                      onClick={() => {
                        history.push(`/app/customer/${customer.id}/bimeList`);
                      }}
                    >
                      <EditTwoToneIcon variant="primary" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div style={{marginTop: "5px"}}>
        <Pagination
          className="pagination-primary"
          count={customerPagedList && customerPagedList.totalPages}
          onChange={(e, p) => onPageChange(p)}
        />
      </div>
    </>
  );
}
