import { Divider, List, makeStyles, withStyles } from "@material-ui/core";
import React from "react";
import { mainListItems, secondaryListItems } from "./listItems";
// for convert JSS Object to css => withStyles & makeStyles

const useStyles = makeStyles((theme) => ({
  sideMenu: {
    display: "flex",
    flexDirection: "column",
    position: "fixed",
    left: "0px",
    width: "250px",
    height: "100%",
    backgroundColor: theme.palette.secondary.light,
  },
}));

const SideMenu = () => {
  const classes = useStyles();
  return (
    <div className={classes.sideMenu}>
        <List>{mainListItems}</List>
      <Divider />
      <List>{secondaryListItems}</List>
    </div>
  );
};

export default SideMenu;
