import { CssBaseline, ThemeProvider } from "@material-ui/core";
import {
  jssPreset,
  makeStyles,
  StylesProvider,
} from "@material-ui/core/styles";
import "./App.css"
import "bootstrap/dist/css/bootstrap.min.css";
import { create } from "jss";
import rtl from "jss-rtl";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { auth } from "redux/actions/authAction";
import { history, request } from "shared/helpers/APIUtils";
import "../assets/fonts/css/fontiran.css";
import Header from "../Containers/Header/Header";
import SideMenu from "../Containers/SideMenu/SideMenu";
import { Router } from "react-router-dom";
import ScrollToTop from "utils/ScrollToTop";
import { Routes } from "./Router/Routes";
import CustomTheme from "assets/CustomTheme";
import { ACCESS_TOKEN } from "../shared/helpers/APIUtils";

const useStyles = makeStyles({
  appMain: {
    paddingRight: "250px",
    width: "100%",
  },
});

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

function App() {
  const classes = useStyles();

  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchUser = async () => {
      if (localStorage.getItem(ACCESS_TOKEN)) {
        try {
          const user = await request().get("/user/me");
          dispatch(auth(user.data));
        } catch (error) {
          console.log(error);
        }
      }
      setLoading(false);
    };
    fetchUser();
  }, []);
  console.log(loading)

  return (
    <Router history={history} basename="/">
      <ScrollToTop>
        <ThemeProvider theme={CustomTheme}>
          <StylesProvider jss={jss}>
            <SideMenu />
            <div className={classes.appMain}>
              <Header />
              <Routes loading={loading} />
            </div>
            <CssBaseline />
          </StylesProvider>
        </ThemeProvider>
      </ScrollToTop>
    </Router>
  );
}

export default App;
