import React from "react";
import {
  GridList,
  GridListTile,
  GridListTileBar,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import EditTwoToneIcon from "@material-ui/icons/EditTwoTone";
import IconButton from "@material-ui/core/IconButton";
import DeleteForeverTwoToneIcon from "@material-ui/icons/DeleteForeverTwoTone";
import moment from "moment-jalaali";
import { PUBLIC_FOLDER_PATH } from "shared/helpers/APIUtils";
import ImageSlider from "../../../shared/components/imageSilder/ImageSlider";

const tableHeadCells = [
  { id: "ghestNumber", label: "شماره قسط" },
  { id: "ghestDate", label: "تاریخ قسط" },
  { id: "ghestPrice", label: "مبلغ قسط" },
  { id: "imageUrl", label: "مستندات" },
  { id: "smsStatus", label: "وضعیت پیامک" },
  { id: "note", label: "یادداشت" },
  { id: "contractsActions", label: "حذف / ویرایش" },
];

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 250,
    height: 190,
  },
  image: {
    width: "100%",
    height: "100%",
  },
}));

export default function GhestListTable(props) {
  const { ghestList, onEditeGhest, onDeleteGhest } = props;

  const classes = useStyles();
  return (
    <TableContainer component={Paper} style={{ marginTop: "10px" }}>
      <Table>
        <TableHead>
          {tableHeadCells.map((headCell) => (
            <TableCell key={headCell.id}>{headCell.label}</TableCell>
          ))}
        </TableHead>
        <TableBody>
          {ghestList &&
            ghestList.length > 0 &&
            ghestList.map((ghest) => (
              <TableRow key={ghest.id}>
                <TableCell>{ghest.ghestNumber}</TableCell>
                <TableCell>
                  {moment(ghest.ghestDate).format("jYYYY/jMM/jDD")}
                </TableCell>
                <TableCell>{ghest.ghestPrice}</TableCell>
                <TableCell>
                  <ImageSlider
                    slides={ghest.imageUrl && JSON.parse(ghest.imageUrl)}
                  />
                </TableCell>
                <TableCell>{ghest.smsStatus ? ghest.smsStatus : ""}</TableCell>
                <TableCell
                  style={{
                    overflow: "auto",
                    width: "200px",
                    height: "150px",
                  }}
                >
                  {ghest.note}
                </TableCell>
                <TableCell>
                  <IconButton
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={() => onEditeGhest(ghest.id)}
                  >
                    <EditTwoToneIcon />
                  </IconButton>
                  <IconButton
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={() => onDeleteGhest(ghest)}
                  >
                    <DeleteForeverTwoToneIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
