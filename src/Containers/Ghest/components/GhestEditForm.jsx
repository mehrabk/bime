import React, { useEffect, useState } from "react";
import { useForm, Controller, useFieldArray } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { ScaleLoader } from "react-spinners";
import {
  Button,
  Container,
  DialogActions,
  DialogContent,
  Grid,
  TextField,
} from "@material-ui/core";
import BlockUi from "react-block-ui";
import { useGhestItem } from "shared/hooks/GhestHooks";
import { request } from "shared/helpers/APIUtils";
import Notification from "shared/components/notification/Notification";
import { NumberVerifier } from "shared/helpers/NumberFormatInput";

import moment from "moment";
import jMoment from "moment-jalaali";
import JalaliUtils from "@date-io/jalaali";
import {
  TimePicker,
  DateTimePicker,
  DatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DropzoneUploader from "shared/components/form/DropzoneUploader";
import { DropzoneRequest } from "../../../shared/helpers/APIUtils";
import ImagePreview from "./ImagePreview";

jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: false });

const VALIDATION_SCHEMA = yup.object().shape({
  ghestNumber: yup.string().required("ضروری"),
  ghestDate: yup.string().required("ضروری"),
  ghestPrice: yup.string().required("ضروری"),
  imageUrl: yup.string(),
  note: yup.string().max(300).required("ضرروری"),
});

const FormImp = ({ methods, ghestItem }) => {
  const [selectedDate, handleDateChange] = useState(moment());

  const { fields, append, remove } = useFieldArray({
    control: methods.control,
    name: "imageArray",
  });

  const handleDropZoneChange = (fileWithMeta, status, xhr) => {
    if (status === "done" && fileWithMeta.file.name !== "imageArray") {
      append(JSON.parse(xhr.response));
    }
    console.log("xhr =>", xhr);
    console.log("fileWithMeta =>", fileWithMeta);
  };

  console.log("imageArray ==== > ", fields);
  methods.watch("ghestNumber");

  useEffect(() => {
    if (ghestItem && ghestItem.imageUrl && ghestItem.imageUrl.length > 0) {
      append(JSON.parse(ghestItem.imageUrl));
    }
  }, []);

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={12} md={6} lg={6} xl={6}>
          <TextField
            variant="outlined"
            autoComplete="off"
            onChange={(e) =>
              methods.setValue("ghestNumber", NumberVerifier(e.target.value))
            }
            size="small"
            autoFocus
            margin="dense"
            label="شماره قسط"
            name="ghestNumber"
            fullWidth
            inputRef={methods.register}
            defaultValue={ghestItem && ghestItem.ghestNumber}
            error={
              methods.errors &&
              methods.errors.ghestNumber &&
              methods.errors.ghestNumber.message
            }
            helperText={
              methods.errors &&
              methods.errors.ghestNumber &&
              methods.errors.ghestNumber.message
            }
          />
        </Grid>

        <Grid item xs={12} md={6} lg={6} xl={6}>
          <TextField
            variant="outlined"
            autoComplete="off"
            onChange={(e) =>
              methods.setValue("ghestPrice", NumberVerifier(e.target.value))
            }
            size="small"
            margin="dense"
            label="مبلغ قسط"
            name="ghestPrice"
            fullWidth
            inputRef={methods.register}
            defaultValue={ghestItem && ghestItem.ghestPrice}
            error={
              methods.errors &&
              methods.errors.ghestPrice &&
              methods.errors.ghestPrice.message
            }
            helperText={
              methods.errors &&
              methods.errors.ghestPrice &&
              methods.errors.ghestPrice.message
            }
          />
        </Grid>

        <Grid item xs={12} md={6} lg={6} xl={6}>
          <MuiPickersUtilsProvider utils={JalaliUtils} locale="fa">
            <DatePicker
              label="تاریخ واریز قسط"
              clearable
              inputVariant="outlined"
              variant="dialog"
              fullWidth
              name="ghestDate"
              inputRef={methods.register}
              okLabel="تأیید"
              cancelLabel="لغو"
              clearLabel="پاک کردن"
              labelFunc={(date) => (date ? date.format("jYYYY/jMM/jDD") : "")}
              value={selectedDate}
              onChange={handleDateChange}
              error={
                methods.errors &&
                methods.errors.ghestDate &&
                methods.errors.ghestDate.message
              }
              helperText={
                methods.errors &&
                methods.errors.ghestDate &&
                methods.errors.ghestDate.message
              }
            />
          </MuiPickersUtilsProvider>
        </Grid>

        <Grid item xs={12} md={6} lg={6} xl={6}>
          <TextField
            multiline
            rowsMax={7}
            variant="outlined"
            autoComplete="off"
            size="small"
            margin="dense"
            label="یادداشت"
            name="note"
            fullWidth
            inputRef={methods.register}
            defaultValue={ghestItem && ghestItem.note}
            error={
              methods.errors &&
              methods.errors.note &&
              methods.errors.note.message
            }
            helperText={
              methods.errors &&
              methods.errors.note &&
              methods.errors.note.message
            }
          />
        </Grid>

        <Grid item xs={12} md={6} lg={6} xl={6}>
          <TextField
            name="imageUrl"
            value={fields && fields.length > 0 ? JSON.stringify(fields) : ""}
            hidden
            inputRef={methods.register}
            // defaultValue={ghestItem && ghestItem.imageUrl}
            error={
              methods.errors &&
              methods.errors.imageUrl &&
              methods.errors.imageUrl.message
            }
            helperText={
              methods.errors &&
              methods.errors.imageUrl &&
              methods.errors.imageUrl.message
            }
          />
          <DropzoneUploader
            name="imageArray"
            accept="image/png, image/jpeg, image/jpg, video/mp4, video/mov"
            uploadParams={DropzoneRequest}
            onChange={handleDropZoneChange}
            maxFiles={2}
            // validate={}
          />
          {fields &&
            fields.length > 0 &&
            fields.map((item) => (
              <ul key={item.id}>
                <ImagePreview item={item} remove={remove} />
              </ul>
            ))}
        </Grid>
      </Grid>
    </>
  );
};

export default function GhestEditForm(props) {
  const { ghestId, bimeId, onUpdateGhest, handleClose } = props;
  const methods = useForm({ resolver: yupResolver(VALIDATION_SCHEMA) });
  const [ghestItem, { errorMsg, isLoading }] = useGhestItem(ghestId);
  const [saving, setSaving] = useState(false);
  const [showNotification, setShowNotification] = useState({
    isOpen: false,
    message: "",
    type: "",
  });

  const onSubmit = async (data) => {
    data.ghestDate = jMoment(data.ghestDate, "jYYYY/jMM/jDD").format(
      "YYYY-MM-DD"
    );
    console.log(data);
    setSaving(true);
    try {
      const response = await request().post(`/bime/${bimeId}/addGhest`, {
        ...data,
        id: ghestId,
      });
      onUpdateGhest(response.data);
      handleClose();
    } catch (error) {
      console.log(error);
      setShowNotification({
        isOpen: true,
        message: "ذخیره سازی قسط انجام نشد",
        type: "error",
      });
    }
    setSaving(false);
  };

  useEffect(() => {
    if (errorMsg && errorMsg !== "") {
      setShowNotification({
        isOpen: true,
        message: errorMsg,
        type: "error",
      });
    }
  }, [errorMsg]);

  return (
    <>
      <BlockUi
        blocking={saving || (isLoading && ghestId > 0)}
        loader={
          <ScaleLoader
            color={"var(--success)"}
            loading={saving || (isLoading && ghestId > 0)}
          />
        }
        tag="div"
      >
        <DialogContent>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            {!isLoading && ghestItem && ghestId > 0 && (
              <FormImp methods={methods} ghestItem={ghestItem} />
            )}
            {isLoading && (!ghestItem || ghestId === 0) && (
              <FormImp methods={methods} />
            )}
            <DialogActions>
              <Button type="submit" variant="contained" color="primary">
                ذخیره
              </Button>
              <Button
                onClick={handleClose}
                variant="contained"
                color="secondary"
              >
                انصراف
              </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </BlockUi>

      <Notification
        showNotification={showNotification}
        onClose={() =>
          setShowNotification({ isOpen: false, message: "", type: "" })
        }
      />
    </>
  );
}
